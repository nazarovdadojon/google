import { __decorate } from "tslib";
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
let SearchService = class SearchService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = environment.SERVER_URL;
        this.api_key = environment.API_KEY;
        this.cx_key = environment.CX_KEY;
    }
    getSearchData(searchTerm) {
        return this.httpClient.get(`${this.url}`, {
            params: {
                key: this.api_key,
                cx: this.cx_key,
                q: searchTerm
            }
        });
    }
};
SearchService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SearchService);
export { SearchService };
//# sourceMappingURL=search.service.js.map