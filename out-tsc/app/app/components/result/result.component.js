import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ResultComponent = class ResultComponent {
    constructor(searchService) {
        this.searchService = searchService;
        this.subs = [];
    }
    ngOnInit() {
        const { term } = history.state;
        this.term = term;
        if (term) {
            this.subs.push(this.searchService.getSearchData(term).subscribe((data) => {
                this.results = data;
            }));
        }
    }
    ngOnDestroy() {
        this.subs.map(s => s.unsubscribe());
    }
    search(form) {
        const { search_term } = form.value;
        this.term = search_term;
        this.subs.push(this.searchService.getSearchData(search_term).subscribe((data) => {
            this.results = data;
            console.log(data);
        }));
    }
};
ResultComponent = __decorate([
    Component({
        selector: 'app-result',
        templateUrl: './result.component.html',
        styleUrls: ['./result.component.scss']
    })
], ResultComponent);
export { ResultComponent };
//# sourceMappingURL=result.component.js.map