import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { NgForm, ControlContainer } from '@angular/forms';
let SearchComponent = class SearchComponent {
    constructor() {
        this.rightIcon = 'false';
    }
    ngOnInit() {
    }
};
__decorate([
    Input()
], SearchComponent.prototype, "currentTermValue", void 0);
__decorate([
    Input()
], SearchComponent.prototype, "rightIcon", void 0);
SearchComponent = __decorate([
    Component({
        selector: 'app-search',
        templateUrl: './search.component.html',
        styleUrls: ['./search.component.scss'],
        viewProviders: [
            {
                provide: ControlContainer, useExisting: NgForm
            }
        ]
    })
], SearchComponent);
export { SearchComponent };
//# sourceMappingURL=search.component.js.map